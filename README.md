# UA Program *(now deprecated: use [UAQS Program](https://bitbucket.org/ua_drupal/uaqs_program) instead)*

## Overview ##
This repository contains a module made with [Features](https://www.drupal.org/project/features) that provides a UA Program content type.

## Requirements ##
- In order to use this feature, you must first download and enable the [Features](https://www.drupal.org/project/features) module.
- Place the feature from this repository into your site's module folder and enable it as you would any other module.
- Dependencies:
  - Drupal Core modules
    - Taxonomy
    - Text
  - Contributed modules
    - [CTools](https://www.drupal.org/project/ctools) used in support of the Strongarm module
    - [StrongArm](https://www.drupal.org/project/strongarm) used to save content type settings

Handy Drush dl/en command:

```
#!

drush en ctools strongarm
```
## Views ##
Not yet known if this feature will contain views.